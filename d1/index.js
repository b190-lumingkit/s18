// mini-activity
/* 
function logUserInput(){
    let nickName = prompt("Enter your nickname");
    console.log(nickName);
}
logUserInput();
*/

// Using parameters to pass data to a function
/* 
    PARAMETER
        -it acts as a named varible/container that exists only inside of a function
        -used to store information that is provided to a function when it is called/invoked;
*/
function printName(name){
    console.log("Hello " + name);
}
// ARGUMENTS
// "Johnny", the information provided directly into the function, is called an argument. Values passed when invoking function are called arguments. These arguments are then stored as the parameters within the function.
printName("Johnny");
printName("Chamber");
printName("Killjoy");

let sampleName = "Yua";
printName(sampleName);

// 2nd mini-activity
function printNumber(num1, num2){
    console.log("The numbers passed as arguments are: ");
    console.log(num1);
    console.log(num2);
}
printNumber(12, 24);

function printFriends(friend1, friend2, friend3){
    console.log("My friends are " + friend1 + ", " + friend2 + ", and " + friend3);
}
printFriends("Chamber", "Sage", "Omen");
// end of 2nd mini-activity

function checkDivisibilityBy8(num){
    let remainder = num%8;
    console.log("The remainder of " + num + " is " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(28);

// 3rd mini-activity
function checkDivisibilityBy4(num){
    let remainder = num%4;
    console.log("The remainder of " + num + " is " + remainder);
    let isDivisibleBy4 = remainder === 0;
    console.log("Is " + num + " divisible by 4?");
    console.log(isDivisibleBy4);
}
checkDivisibilityBy4(123123123);

// Function Debugging Practice
function isEven(num){
    let checkIfEven = num % 2 === 0;
    console.log("Is " + num + " even? " + checkIfEven);
}
isEven(20);
isEven(21);

function isOdd(num){
    let checkIfOdd = num % 2 !== 0;
    console.log("Is " + num + " odd? " + checkIfOdd);
}
isOdd(31);

/* 
    FUNCTIONS AS ARGUMENTS
        function parameters can also accept other functions as arguments
        used in some complex functions
*/
function argumentFunction(){
    console.log("This function is passed into another function.");
}

function invokeFunction(functionParameter){
    functionParameter;
}
// when a function is used with a parenthesis, it denotes that invoking/calling a function
invokeFunction(argumentFunction());

/* function printFullName(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan", "Dela", "Cruz"); */
// works the same way as the function above, but less efficient than a function
/* let firstName = "Juan";
let middleName = "Dela";
let lastName = "Cruz";
console.log(firstName + " " + middleName + " " + lastName); */

// Return Statement
// return statement allows us to output a value from a function passed to the line/block of code that invoked/called our function
function returnFullName(firstName, middleName, lastName){
    let fullName = firstName + " " + middleName + " " + lastName;
    return fullName;
    //console.log("This message will not be printed"); - not printed/executed because ideally any line/block of code that comes after the return statement is ignored, because it ends the function execution
}
console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

// the value returned from a function can also be stored inside a variable
// let fullName = returnFullName("Jeffrey", "Smith", "Bezos");
// console.log(fullName);

// mini-activity
function returnAddress(city, province){
    let fullAddress = city + ", " + province;
    return fullAddress;
}
console.log(returnAddress("Dumaguete", "Negros Oriental"));

function printHeroInfo(heroName, tierLevel, job){
    let heroInfo = "Username: " + heroName + "\nTier Level: " + tierLevel + "\nJob: " + job;
    return heroInfo;
}
let hero = printHeroInfo("Moon Knight", "B", "Assassin");
console.log(hero);